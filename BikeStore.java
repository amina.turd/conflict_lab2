/**
 *author: Amina Turdalieva 
 * id: 2035572
 * date: 2021-09-07
 */

 public class BikeStore {
    public static void main(String[] args) {
        Bicycle[] bikes = new Bicycle[4];
        bikes[0] = new Bicycle("ManOne", 5, 10);
        bikes[1] = new Bicycle("ManTwo", 7, 15);
        bikes[2] = new Bicycle("ManThree", 10, 17.5);
        bikes[3] = new Bicycle("ManFour", 22, 20);

        for (int i = 0; i < bikes.length; i++){
            System.out.println(bikes[i]);
        }
    }
}

