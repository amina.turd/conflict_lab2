
/**
 *author: Amina Turdalieva 
 * id: 2035572
 * date: 2021-09-07
 */

public class Bicycle {

    private String manufacturer;
    private int numberGears;
    private double maxSpeed;

    public String getManufacturer(){
        return this.manufacturer;
    }

    public int getNumberGears(){
        return this.numberGears;
    }

    public double getMaxSpeed(){
        return this.maxSpeed;
    }
    
    public Bicycle(String manufacturer, int numberGears, double maxSpeed){
        this.manufacturer = manufacturer;
        this.numberGears = numberGears;
        this.maxSpeed = maxSpeed;
    }

    public String toString(){
        return "Manufacturer is: " + this.manufacturer + ", Number of Gears is: " + this.numberGears + ", Max Speed is: " + this.maxSpeed;
    }

}
